# systemd-networkd-bridge

Here is an example of how to setup a hypervisor's LAN connection to be a bridged
connection to the local network. Your adapter name may be different, so you can
rename and update these files to match your actual hardware configuration.

- Change the MAC address to match your adapter, and configure the correct
network addresses.
- Put these files in /etc/systemd/network/
- Disable and stop the NetworkManager service.
- Enable and start the systemd-networkd service.

```
systemctl disable --now NetworkManager
systemctl enable --now systemd-networkd
```

Your VMs will then need to be configured for the xenbr0 bridge, and they'll use
their dhcp client to pickup an IP address from your dhcp server. This
configuration does NOT use VLANs. (I can add that as another example.)
